from maven:3.8-jdk-11
RUN apt-get update && apt-get install -y curl sudo

# Node
RUN curl -fsSL https://deb.nodesource.com/setup_15.x | sudo -E bash - && \
  sudo apt-get install -y nodejs && \
  echo "NODE Version:" && node --version && \
  echo "NPM Version:" && npm --version && \
  echo "This image is for pipeline. It has maven3.8-jdk11 and node 14.x as well as openapi-generator-cli installed."

# RUN sudo npm cache clean -f && \
#   sudo npm install -g n && \
#   sudo n stable

RUN npm install @openapitools/openapi-generator-cli -g

