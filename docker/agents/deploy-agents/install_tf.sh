usage() {
  cat 1>&2 <<EOF
Installs the TERRAFORM CLI v2

USAGE:
    install [FLAGS] [OPTIONS]

FLAGS:
    -u, --update              Updates the TERRAFORM CLI v2 if a different version
                              is previously installed. By default, this script
                              will not update the TERRAFORM CLI if a previous
                              installation is detected.

    -h, --help                Prints help information

OPTIONS:
    -i, --install-dir <path>  The directory to install the TERRAFORM CLI v2. By
                              default, this directory is: /usr/local/terraform-cli

    -b, --bin-dir <path>      The directory to store symlinks to executables
                              for the TERRAFORM CLI v2. By default, the directory
                              used is: /usr/local/bin
EOF
}

parse_commandline() {
  while test $# -gt 0
  do
    key="$1"
	case "$key" in
	  -i|--install-dir)
	    PARSED_INSTALL_DIR="$2"
		shift
	   ;;
	  -b|--bin-dir)
	    PARSED_BIN_DIR="$2"
		shift
	   ;;
	  -u|--update)
	    PARSED_UPGRADE="yes"
	  ;;
	  -h|--help)
	    usage
        exit 0
	  ;;
	  *)
	   die "Got an unexpected argument: $1"
	  ;;
    esac
	shift
  done
}

set_global_vars() {
  ROOT_INSTALL_DIR=${PARSED_INSTALL_DIR:-/usr/local/terraform-cli}
  BIN_DIR=${PARSED_BIN_DIR:-/usr/local/bin}
  UPGRADE=${PARSED_UPGRADE:-no}

  EXE_NAME="terraform"
  INSTALLER_DIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"
  INSTALLER_EXE="$INSTALLER_DIST_DIR/$EXE_NAME"
  TERRAFORM_EXE_VERSION=$($INSTALLER_EXE --version | cut -d ' ' -f 1 | cut -d '/' -f 2)

  INSTALL_DIR="$ROOT_INSTALL_DIR/v2/$TERRAFORM_EXE_VERSION"
  INSTALL_DIR="$INSTALL_DIR"
  INSTALL_DIST_DIR="$INSTALL_DIR/dist"
  INSTALL_BIN_DIR="$INSTALL_DIR/bin"
  INSTALL_TERRAFORM_EXE="$INSTALL_BIN_DIR/$EXE_NAME"

  CURRENT_INSTALL_DIR="$ROOT_INSTALL_DIR/v2/current"
  CURRENT_TERRAFORM_EXE="$CURRENT_INSTALL_DIR/bin/$EXE_NAME"

  BIN_TERRAFORM_EXE="$BIN_DIR/$EXE_NAME"
}

create_install_dir() {
  mkdir -p "$INSTALL_DIR" || exit 1
  {
    setup_install_dist &&
    setup_install_bin &&
    create_current_symlink
  } || {
    rm -rf "$INSTALL_DIR"
    exit 1
  }
}

check_preexisting_install() {
  if [ -L "$CURRENT_INSTALL_DIR" ] && [ "$UPGRADE" = "no" ]
  then
    die "Found preexisting TERRAFORM CLI installation: $CURRENT_INSTALL_DIR. Please rerun install script with --update flag."
  fi
  if [ -d "$INSTALL_DIR" ]
  then
    echo "Found same TERRAFORM CLI version: $INSTALL_DIR. Skipping install."
    exit 0
  fi
}

setup_install_dist() {
  cp -r "$INSTALLER_DIST_DIR" "$INSTALL_DIST_DIR"
}

setup_install_bin() {
  mkdir -p "$INSTALL_BIN_DIR"
  ln -s "../dist/$EXE_NAME" "$INSTALL_TERRAFORM_EXE"
}

create_current_symlink() {
  ln -snf "$INSTALL_DIR" "$CURRENT_INSTALL_DIR"
}

create_bin_symlinks() {
  mkdir -p "$BIN_DIR"
  ln -sf "$CURRENT_TERRAFORM_EXE" "$BIN_TERRAFORM_EXE"
}

die() {
	err_msg="$1"
	echo "$err_msg" >&2
	exit 1
}

main() {
  parse_commandline "$@"
  set_global_vars
  check_preexisting_install
  create_install_dir
  create_bin_symlinks
  echo "You can now run: $BIN_TERRAFORM_EXE --version"
  exit 0
}

main "$@" || exit 1
