FROM jenkins/inbound-agent:jdk11

USER root

RUN apt-get update && \
    apt-get install jq -y && \
    apt-get install gettext-base -y && \
    apt-get install curl -y && \
    apt-get install -y wget && \
    apt-get install -y unzip && \
    apt-get install unzip -y && \
    rm -rf /var/lib/apt/lists/*

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && unzip awscliv2.zip && ./aws/install

RUN wget --quiet https://releases.hashicorp.com/terraform/1.1.7/terraform_1.1.7_linux_amd64.zip && \
  unzip terraform_1.1.7_linux_amd64.zip && \
  mv terraform /usr/bin && \
  rm terraform_1.1.7_linux_amd64.zip


USER ${user}
