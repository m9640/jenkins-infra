FROM jenkins/inbound-agent:jdk11
USER root
RUN apt-get update && apt-get install -y curl sudo
RUN curl -fsSL https://deb.nodesource.com/setup_17.x | sudo -E bash - && \
  sudo apt-get install -y nodejs && \
  echo "NODE Version:" && node --version && \
  echo "NPM Version:" && npm --version
RUN npm install -g npm@8.3.0 && npm --version

RUN apt-get update && \
    apt-get install jq -y && \
    apt-get install gettext-base -y && \
    apt-get install curl -y && \
    apt-get install unzip -y

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && unzip awscliv2.zip && ./aws/install

USER ${user}
