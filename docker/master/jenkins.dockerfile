FROM jenkins/jenkins:2.332.1-lts-jdk11


# tricks looked at:
# https://www.popularowl.com/jenkins/automating-jenkins-install-docker-terraform/
# Jenkins runs all grovy files from init.groovy.d dir
# use this for creating default admin user, and pointing to the correct external URL. (only does not work locally?)
COPY ./jenkins-plugins /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

COPY initial-config.groovy /usr/share/jenkins/ref/init.groovy.d/
COPY disable_jobdsl_security.groovy /usr/share/jenkins/ref/init.groovy.d/
COPY jenkins.yaml /usr/share/jenkins/ref/jenkins.yaml
COPY jenkins.yaml /config-backup/jenkins.yaml
COPY jobs/seedjob/seedjob.xml /usr/share/jenkins/ref/jobs/seedjob/config.xml


ENV JENKINS_USER jenkins-user
ENV JENKINS_PASSWORD jenkins-password
ENV GITLAB_USER gitlab-user
ENV GITLAB_PASSWORD gitlab-password
ENV GITLAB_API_TOKEN gitlab-token
ENV JENKINS_URL jenkins-url

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

RUN echo 2.0 > /usr/share/jenkins/ref/jenkins.install.InstallUtil.lastExecVersion

# Note : the pipelines git repo is hardcoded in seedjob.xml
